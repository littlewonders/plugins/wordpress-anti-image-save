# Wordpress Anti Image Save

A very simple Wordpress plugin which registers a Gutenberg block. This block will add a banner to the viewer's page warning them from copying the page content.