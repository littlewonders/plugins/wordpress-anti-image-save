wp.blocks.registerBlockType('lw/anti-theft', {
  title: 'Anti Save',
  icon: 'lock',
  category: 'common',
  attributes: {},

  edit: function (props) {
    return React.createElement(
      "div",
      null,
      React.createElement(
        "h5",
        { style: { backgroundColor: '#9fd2ff' } },
        "LittleWonders Anti-Theft Module. This element does not have any configuration options."
      )
    );
  },
  save: function (props) {
    return wp.element.createElement(
      "input",
      { type: 'hidden', 'name': 'is-anti-theft', value: 'yes' },
      null
    );
  }
})