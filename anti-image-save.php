<?php
/**
 * Plugin Name: [LW] Anti Image Save
 * Description: A plugin designed for the Little Wonders wordpress instance which adds anti-theft measures to gallery pages
 * Version: 1.0
 * Author: Jay Williams <admin@jaywilliams.me>
 */

function anti_image_save_load()
{
    wp_enqueue_script(
        'anti-image-save',
        plugin_dir_url(__FILE__) . 'block.js',
        array('wp-blocks', 'wp-editor'),
        true
    );
}

add_action('enqueue_block_editor_assets', 'anti_image_save_load');

function anti_image_save_load_frontend()
{
    wp_enqueue_script(
        'anti-image-save-frontend',
        plugin_dir_url(__FILE__) . 'check.js',
        [],
        '1',
        true
    );
}

add_action('wp_enqueue_scripts', 'anti_image_save_load_frontend');
