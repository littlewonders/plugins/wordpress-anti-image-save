if (document.querySelectorAll("[name=is-anti-theft]").length) {
    document.addEventListener('contextmenu', event => event.preventDefault());
    let styles = document.createElement("style");
    styles.innerHTML = `
        .anti-theft-warning-element {
            position: sticky;
            bottom: 0px;
            left: 0px;
            width: 100%;
            background-color: red;
            color: white;
            padding: 5px;
            z-index: 1000;
            text-align: center;
        }

        @media screen and (max-width: 600px) {
            .anti-theft-warning-element {
                font-size: 12px;
            }
        }
    `
    let warningElement = document.createElement("div");
    warningElement.innerText = "All images on this page are the property of Little Wonders Photography, and unauthorized copying or distribution is forbidden. All access to this page is monitored and Little Wonders reserves the right to disable the page following mis-use or distribution of this link."
    warningElement.classList.add("anti-theft-warning-element");
    document.querySelector("body").appendChild(warningElement);
    document.querySelector("body").appendChild(styles);
}